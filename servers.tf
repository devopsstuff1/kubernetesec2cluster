#Bastion
resource "aws_instance" "bastion" {
  ami                         = var.ami_id
  instance_type               = var.bastion_instance_type
  subnet_id                   = module.vpc.public_subnets[0]
  associate_public_ip_address = "true"
  vpc_security_group_ids      = [aws_security_group.allow_ssh.id]
  key_name                    = aws_key_pair.k8_ssh.key_name
  user_data                   = <<-EOF
#!bin/bash

# echo "PubkeyAcceptedKeyTypes=+ssh-rsa" >> /etc/ssh/sshd_config.d/10-insecure-rsa-keysig.conf
# systemctl reload sshd

echo "${tls_private_key.ssh.private_key_openssh}" > /home/ubuntu/.ssh/id_ed25519
chown ubuntu /home/ubuntu/.ssh/id_ed25519
chgrp ubuntu /home/ubuntu/.ssh/id_ed25519
chmod 600   /home/ubuntu/.ssh/id_ed25519

echo "starting ansible install"
apt-add-repository ppa:ansible/ansible -y
apt update
apt install ansible -y

apt install python-jmespath
sudo -u ubuntu ansible-galaxy collection install community.general

/bin/echo "Hello World" >> /tmp/testfile.txt

EOF

  tags = {
    Name = "Bastion"
  }
}

#Master
resource "aws_instance" "masters" {
  count                  = var.master_node_count
  ami                    = var.ami_id
  instance_type          = var.master_instance_type
  subnet_id              = element(module.vpc.private_subnets, count.index)
  key_name               = aws_key_pair.k8_ssh.key_name
  vpc_security_group_ids = [aws_security_group.k8_nodes.id, aws_security_group.k8_masters.id]

  tags = {
    Name = format("Master-%02d", count.index + 1)
  }
}

#Worker
resource "aws_instance" "workers" {
  count                  = var.worker_node_count
  ami                    = var.ami_id
  instance_type          = var.worker_instance_type
  subnet_id              = element(module.vpc.private_subnets, count.index)
  key_name               = aws_key_pair.k8_ssh.key_name
  vpc_security_group_ids = [aws_security_group.k8_nodes.id, aws_security_group.k8_workers.id]

  tags = {
    Name = format("Worker-%02d", count.index + 1)
  }
}
