terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.36.1"
    }
    tls = {
      source  = "hashicorp/tls"
      version = "4.0.3"
    }
    local = {
      source  = "hashicorp/local"
      version = "2.2.3"
    }
    null = {
      source  = "hashicorp/null"
      version = "3.1.1"
    }
    time = {
      source  = "hashicorp/time"
      version = "0.9.0"
    }
  }
}

provider "aws" {
  region     = var.AWS_REGION
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}

provider "tls" {
  # Configuration options
}

provider "local" {
  # Configuration options
}

provider "null" {
  # Configuration options
}

provider "time" {
  # Configuration options
}
