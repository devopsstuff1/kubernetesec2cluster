resource "tls_private_key" "ssh" {
  algorithm = "ED25519"
}

resource "local_file" "k8_ssh_key" {
  filename        = "k8_ssh_key"
  file_permission = "600"
  content         = tls_private_key.ssh.private_key_openssh
}

resource "aws_key_pair" "k8_ssh" {
  key_name   = "k8_ssh"
  public_key = tls_private_key.ssh.public_key_openssh
}
