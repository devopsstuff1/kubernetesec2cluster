variable "AWS_REGION" {
  description = "aws region"
  default     = "eu-west-3"
}

variable "aws_access_key" {
  description = "aws access key"
  type        = string
  sensitive   = true
}

variable "aws_secret_key" {
  description = "aws secret key"
  type        = string
  sensitive   = true
}

variable "ami_id" {
  description = "ami id"
  type        = string
  default     = "ami-0759231d7c53fc8e7"
}

variable "availability_zones" {
  description = "availability zones"
  type        = list(string)
  default     = ["eu-west-3a", "eu-west-3b", "eu-west-3c"]
}

variable "vpc_cidr" {
  description = "network subnet"
  type        = string
  default     = "10.0.0.0/16"
}

variable "private_subnets" {
  description = "private subnet list"
  type        = list(string)
  default     = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
}

variable "public_subnets" {
  description = "public subnet list"
  type        = list(string)
  default     = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]
}

variable "master_node_count" {
  description = "kubernetes master node count"
  type        = number
  default     = 3
}

variable "worker_node_count" {
  description = "kubernetes worker node count"
  type        = number
  default     = 3
}

variable "ssh_user" {
  description = "ssh user"
  type        = string
  default     = "ubuntu"
}

variable "master_instance_type" {
  description = "master EC2 instance type"
  type        = string
  default     = "t2.micro"
}

variable "worker_instance_type" {
  description = "worker EC2 instance type"
  type        = string
  default     = "t2.micro"
}

variable "bastion_instance_type" {
  description = "bastion EC2 instance type"
  type        = string
  default     = "t2.micro"
}
