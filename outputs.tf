output "bastion_host_public_ip" {
  value = aws_instance.bastion.public_ip
}

output "aws_key_type" {
  value = aws_key_pair.k8_ssh.key_type
}
